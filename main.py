# -*- coding: utf-8 -*-

class RomanNumeral(object):
    ROMAN_ARABIC = {
        'I': 1,
        'V': 5,
        'X': 10,
        'L': 50,
        'C': 100,
        'D': 500,
        'M': 1000,
    }

    ARABIC_ROMAN = zip(
        (1000, 900, 500, 400, 100, 90, 50, 40, 10, 9, 5, 4, 1),
        ('M', 'CM', 'D', 'CD', 'C', 'XC', 'L', 'XL', 'X', 'IX', 'V', 'IV', 'I')
    )

    def roman_to_arabic(self, number):
        if not number:
            raise ValueError()
        res = []
        for x in number:
            x = str(x).strip()
            x = self.ROMAN_ARABIC.get(x, None)
            if x is None:
                raise ValueError()
            if not res:
                res.append(x)
            else:
                if res[-1] < x:
                    res[-1] = x - res[-1]
                else:
                    res.append(x)
        if res:
            return sum(res)
        raise ValueError()

    def arabic_to_roman(self, number):
        res = []
        for integer, numeral in self.ARABIC_ROMAN:
            count = int(number / integer)
            res.append(numeral * count)
            number -= integer * count
        return ''.join(res)

    def __init__(self, number):
        self._number, self._roman = self.get_numbers_tuple(number)

    def get_numbers_tuple(self, number):
        if isinstance(number, int):
            return number, self.arabic_to_roman(number)
        elif isinstance(number, (str, unicode, bytes)):
            return self.roman_to_arabic(number), str(number).strip()
        elif isinstance(number, self.__class__):
            return number._number, number._roman
        else:
            raise ValueError()

    def __repr__(self):
        return '%s (%s)' % (self._roman, str(self._number))

    def __add__(self, other):
        number, roman = self.get_numbers_tuple(other)
        return self.__class__(self._number + number)

    def __radd__(self, other):
        return self.__add__(other)

    def __sub__(self, other):
        number, roman = self.get_numbers_tuple(other)
        return self.__class__(self._number - number)

    def __rsub__(self, other):
        return self.__sub__(other) * -1

    def __mul__(self, other):
        number, roman = self.get_numbers_tuple(other)
        return self.__class__(self._number * number)

    def __rmul__(self, other):
        return self.__mul__(other)

    def __div__(self, other):
        number, roman = self.get_numbers_tuple(other)
        return self.__class__(self._number / number)


if __name__ == '__main__':
    x1944 = RomanNumeral('MCMXLIV')
    x1954 = RomanNumeral('MCMLIV')
    xMCMXLIV = RomanNumeral(1944)
    xMCMLIV = RomanNumeral(1954)

    xSum = x1954 + x1944
    assert xSum._number == 1954 + 1944
    assert (x1954 + 1944)._number == 1954 + 1944
    assert (1944 + x1954)._number == 1954 + 1944
    assert (x1954 + 'MCMXLIV')._number == 1954 + 1944
    assert ('MCMXLIV' + x1954)._number == 1954 + 1944
    xDif = x1954 - x1944
    assert xDif._number == 1954 - 1944
    assert (x1954 - 1944)._number == 1954 - 1944
    assert (1954 - x1944)._number == 1954 - 1944
    assert (x1954 - 'MCMXLIV')._number == 1954 - 1944
    assert ('MCMLIV' - x1944)._number == 1954 - 1944
    xMul = x1954 * x1944
    assert xMul._number == 1954 * 1944
    assert (x1954 * 1944)._number == 1954 * 1944
    assert (1954 * x1944)._number == 1954 * 1944
    assert (x1954 * 'MCMXLIV')._number == 1954 * 1944
    assert ('MCMLIV' * x1944)._number == 1954 * 1944
    xDiv = x1954 / x1944
    assert xDiv._number == 1954 / 1944
    assert (x1954 / 1944)._number == 1954 / 1944
    assert (x1954 / 'MCMXLIV')._number == 1954 / 1944

    print x1944
    print xMCMXLIV
    print x1954
    print xMCMLIV

    print xSum
    print xDif
    print xMul
    print xDiv
